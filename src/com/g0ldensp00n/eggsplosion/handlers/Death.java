package com.g0ldensp00n.eggsplosion.handlers;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.plugin.Plugin;

public class Death implements Listener {
    public Death(Plugin plugin) {
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void playerTakeDamage(EntityDamageEvent entityDamageEvent) {
        entityDamageEvent.setCancelled(true);
        if (entityDamageEvent.getEntity().getType().equals(EntityType.PLAYER)) {
            Player player = (Player) entityDamageEvent.getEntity();
            if (player.getHealth() - entityDamageEvent.getDamage() >= 0) {
                player.setHealth(20);
                player.playSound(player.getLocation(), Sound.ENTITY_BAT_DEATH, 1, 1);
                player.teleport(player.getWorld().getSpawnLocation());
            } else {
                player.setHealth(player.getHealth() - entityDamageEvent.getDamage());
            }
        }
    }
}
