package com.g0ldensp00n.eggsplosion.handlers;

import com.g0ldensp00n.eggsplosion.EggSplosion;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class ExplosionRegen implements Listener {

    private Location location;
    private Material material;
    private byte data;
    private Inventory inventory;
    private List<ExplosionRegen> blockQueue = new ArrayList<>();
    private List<ExplosionRegen> transparentQueue = new ArrayList<>();
    private List<Inventory> blockInventory = new ArrayList<>();
    private Random random;

    private EggSplosion plugin;

    public ExplosionRegen(EggSplosion plugin) {
        this.plugin = plugin;
        Bukkit.getPluginManager().registerEvents(this, plugin);
    }

    private ExplosionRegen(Block block) {
        this.location = block.getLocation();
        this.material = block.getType();
        this.data = block.getData();
        if (block.getType() == Material.CHEST) {
            Chest chest = (Chest) block.getState();
            inventory = Bukkit.createInventory(null, 27, "" + chest.getLocation().getBlockX());
            if (chest.getInventory().getSize() <= 27) {
                inventory.setContents(chest.getBlockInventory().getContents());
            } else {
                Bukkit.getLogger().info("Double chest -> WIP");
            }

        }
    }

    public void update() {
        location.getBlock().setTypeIdAndData(material.getId(), data, true);
        if (location.getBlock().getType().equals(Material.CHEST)) {
            Chest chest = (Chest) location.getBlock().getState();
            chest.getInventory().setContents(inventory.getContents());
        }
    }

    @EventHandler
    public void explosionRegeneration(BlockExplodeEvent event) {
        final int delay = 20 * 3;
        event.setCancelled(true);
        List<ExplosionRegen> blockInfo = new ArrayList<>();
        List<ExplosionRegen> transparentBlocks = new ArrayList<>();
        List<Block> blocks = event.blockList();
        blocks.forEach(block -> {
            if (block.getType().isBlock()) {
                if (!block.getType().equals(Material.TNT) && !block.getType().equals(Material.STANDING_BANNER)) {
                    blockInfo.add(new ExplosionRegen(block));
                    block.setType(Material.AIR);
                } else if (block.getType().equals(Material.TNT)) {
                    blockInfo.add(new ExplosionRegen(block));
                    block.setType(Material.AIR);
                    TNTPrimed tntPrimed = block.getWorld().spawn(block.getLocation(), TNTPrimed.class);
                    tntPrimed.setFuseTicks(20);
                }
            } else {
                transparentBlocks.add(new ExplosionRegen(block));
                block.setType(Material.AIR);
            }
        });
        blockQueue.addAll(blockInfo);
        transparentQueue.addAll(transparentBlocks);

        new BukkitRunnable() {
            int indexBlocks = 0;
            int indexTransparents = 0;

            @Override
            public void run() {
                if (blockInfo.isEmpty()) cancel();
                if (indexBlocks != blockInfo.size()) {
                    blockInfo.get(indexBlocks++).update();
                } else if (indexBlocks == blockInfo.size()) {
                    if (!transparentBlocks.isEmpty()) {
                        transparentBlocks.get(indexTransparents++).update();
                        if (indexTransparents == transparentBlocks.size()) {
                            blockInfo.clear();
                            transparentBlocks.clear();
                            cancel();
                        }
                    } else {
                        cancel();
                    }
                }
            }
        }.runTaskTimer(this.plugin, delay, (long) 0.01);
    }

    @EventHandler
    public void explosionRegeneration(EntityExplodeEvent event) {
        final int delay = 20 * 3;
        event.setCancelled(true);
        List<ExplosionRegen> blockInfo = new ArrayList<>();
        List<ExplosionRegen> transparentBlocks = new ArrayList<>();
        List<Block> blocks = event.blockList();
        blocks.forEach(block -> {
            if (block.getType().isBlock()) {
                if (!block.getType().equals(Material.TNT) && !block.getType().equals(Material.STANDING_BANNER)) {
                    blockInfo.add(new ExplosionRegen(block));
                    block.setType(Material.AIR);
                } else if (block.getType().equals(Material.TNT)) {
                    blockInfo.add(new ExplosionRegen(block));
                    block.setType(Material.AIR);
                    TNTPrimed tntPrimed = block.getWorld().spawn(block.getLocation(), TNTPrimed.class);
                    tntPrimed.setFuseTicks(20);
                }
            } else {
                transparentBlocks.add(new ExplosionRegen(block));
                block.setType(Material.AIR);
            }
        });
        blockQueue.addAll(blockInfo);
        transparentQueue.addAll(transparentBlocks);

        new BukkitRunnable() {
            int indexBlocks = 0;
            int indexTransparents = 0;

            @Override
            public void run() {
                if (blockInfo.isEmpty()) cancel();
                if (indexBlocks != blockInfo.size()) {
                    blockInfo.get(indexBlocks++).update();
                } else if (indexBlocks == blockInfo.size()) {
                    if (!transparentBlocks.isEmpty()) {
                        transparentBlocks.get(indexTransparents++).update();
                        if (indexTransparents == transparentBlocks.size()) {
                            blockInfo.clear();
                            transparentBlocks.clear();
                            cancel();
                        }
                    } else {
                        cancel();
                    }
                }
            }
        }.runTaskTimer(this.plugin, delay, (long) 1);
    }

    public void repairAll() {
        Iterator<ExplosionRegen> blocksQueue = blockQueue.iterator();
        while (blocksQueue.hasNext()) {
            ExplosionRegen explosionRegenBlock = blocksQueue.next();
            explosionRegenBlock.update();
            blocksQueue.remove();
        }
        Iterator<ExplosionRegen> transparentsQueue = transparentQueue.iterator();
        while (blocksQueue.hasNext()) {
            ExplosionRegen explosionRegenTrans = transparentsQueue.next();
            explosionRegenTrans.update();
            transparentsQueue.remove();
        }
    }
}