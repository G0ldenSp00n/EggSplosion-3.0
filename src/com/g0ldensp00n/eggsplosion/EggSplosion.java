package com.g0ldensp00n.eggsplosion;

import com.g0ldensp00n.eggsplosion.handlers.Death;
import com.g0ldensp00n.eggsplosion.handlers.EggExplode;
import com.g0ldensp00n.eggsplosion.handlers.ExplosionRegen;
import com.g0ldensp00n.eggsplosion.handlers.Weapon;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class EggSplosion extends JavaPlugin{
    private String versionNumber;
    private ExplosionRegen explosionRegen;

    @Override
    public void onEnable() {
        versionNumber = Bukkit.getServer().getPluginManager().getPlugin("EggSplosion").getDescription().getVersion();
        getLogger().info("EggSplosion v" + versionNumber + " has been Enabled.");
        explosionRegen = new ExplosionRegen(this);
        EggExplode eggExplode = new EggExplode(this);
        Weapon weapon = new Weapon(this);
        Death death = new Death(this);
    }

    @Override
    public void onDisable(){
        explosionRegen.repairAll();
        getLogger().info("EggSplosion v" + versionNumber + " has been Disabled");
    }
}
